require 'rails_helper'

RSpec.describe 'GPS', type: :feature do
  context 'when vehicle exists on db', js: true do
    it 'shows most recent cords of vehicle in map' do
      vehicle = create :vehicle
      waypoint = create :waypoint, vehicle_identifier: vehicle.id
      visit "/show/#{vehicle.id}"
      expect(page).to have_css 'div#map'
      map_coords = page.find('div#map').first('a')['href']
      target_coords = Rack::Utils.parse_query URI(map_coords).query
      longitude, latitude = target_coords['ll'].split ','
      expect(latitude).to eq waypoint.latitude
      expect(longitude).to eq waypoint.longitude
    end
  end

  # rubocop:disable Metrics/LineLength
  context 'when vehicle does not exist on db' do
    it 'does not show the map, but a message' do
      visit '/show/non_existent_vehicle'
      expect(page).to have_content 'Vehicle does not exist or does not register waypoints'
    end
  end
end
