require 'rails_helper'

# rubocop:disable Metrics/BlockLength
RSpec.describe 'GPS', type: :request do
  let(:vehicle) { create :vehicle }
  before { vehicle }

  describe 'GET /show' do
    context 'when vehicle has a waypoint' do
      let(:waypoint) { create :waypoint, vehicle_identifier: vehicle.id }
      before do
        waypoint
        get "/show/#{vehicle.id}"
      end

      it 'returns a 200 status code' do
        expect(response).to have_http_status 200
      end

      it 'returns the longitude and latitude' do
        expect(response.body).to match(/#{waypoint.latitude}/)
        expect(response.body).to match(/#{waypoint.longitude}/)
      end
    end

    context 'when vehicle has not waypoint' do
      it 'returns an error message' do
        get '/show/non_existent_waypoint'
        re = /Vehicle does not exist or does not register waypoints/
        expect(response.body).to match re
      end
    end
  end

  describe 'POST /api/v1/gps' do
    let(:invalid_attributes) do
      {
        params: {
          waypoint: {
            latitude: 20.23,
            longitude: -0.56,
            sent_at: '2016-06-02 20:45:00',
            vehicle_identifier: ''
          }
        }
      }
    end
    context 'when vehicle identifier is not registerd' do
      context 'when attributes are valid' do
        it 'responds with 204 status code' do
          post '/api/v1/gps', params: {
            waypoint: {
              latitude: 20.23, sent_at: '2016-06-02 20:45:00',
              vehicle_identifier: vehicle.id, longitude: -0.56
            }
          }
          expect(response).to have_http_status 204
        end
      end

      context 'when attributes are invalid' do
        it 'does not create a new waypoint' do
          expect do
            post '/api/v1/gps', invalid_attributes
          end.to change(Waypoint, :count).by 0
          expect(response).to have_http_status 422
        end
      end
    end

    context 'when vehicle identifier is not registerd' do
      context 'with valid attributes' do
        it 'creates a new vehicle and waypoint associated' do
          post '/api/v1/gps', params: {
            waypoint: {
              latitude: 20.23, sent_at: '2016-06-02 20:45:00',
              vehicle_identifier: 'new_identifier', longitude: -0.56
            }
          }
          expect(response).to have_http_status 204
        end
      end

      context 'with invalid attributes' do
        it 'does not create a new register but renders a message' do
          expect do
            post '/api/v1/gps', invalid_attributes
          end.to change(Waypoint, :count).by 0
          expect(response).to have_http_status 422
        end
      end
    end
  end
end
