require 'rails_helper'

RSpec.describe Vehicle, type: :model do
  subject { build :vehicle }

  context 'is valid' do
    it 'with valid attributes' do
      expect(subject).to be_valid
    end
  end

  context 'is invalid' do
    it 'without a vehicle_identifier' do
      subject.vehicle_identifier = nil
      expect(subject).to_not be_valid
    end

    it 'with a vehicle_identifier already taken' do
      subject.save
      new_vehicle = subject.dup
      expect(new_vehicle).to_not be_valid
    end
  end
end