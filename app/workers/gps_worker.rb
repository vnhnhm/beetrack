require 'json'

# GpsWorker - handles only the post to api/v1/gps#gps
class GpsWorker
  include Sidekiq::Worker

  # Receives the waypoint_params, parses it, and creates a new Waypoint record.
  def perform(waypoint_params)
    Waypoint.create JSON.parse(waypoint_params)
  end
end
