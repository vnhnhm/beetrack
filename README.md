# README

```
$ rails db:create
$ rails db:migrate
$ rails db:seed
$ rails server
$ redis-server
$ sidekiq
```

Request:

```js
{
	"waypoint": {
		"longitude": "-33.4074994",
		"latitude": "-70.8888494",
		"sent_at": "2016-06-02 20:45:00",
		"vehicle_identifier": "AS1293"
	}
}
```
