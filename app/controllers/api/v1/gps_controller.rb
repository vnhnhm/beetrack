module Api
  module V1
    # Gps Controller handles the creation of waypoints and the way of show them
    class GpsController < ApplicationController
      skip_before_action :verify_authenticity_token
      before_action :find_or_create_vehicle, only: :gps

      # Creates a waypoint instance variable with all the waypoints
      # equal to the params[:id] being passed.
      def show
        @waypoint = Waypoint.where('vehicle_identifier = ?', params[:id])
                            .select(:latitude, :longitude)
      end

      # Initializes the GpsWorker as an async perform passing the
      # waypoint_params as json.
      def gps
        GpsWorker.perform_async waypoint_params.to_json
      end

      private

      def waypoint_params
        params.require(:waypoint).permit :longitude, :latitude, :sent_at,
                                         :vehicle_identifier
      end

      # Creates the hash used in find_or_create_vehicle to look for a record
      # with the same values or otherwise create a new one.
      def identifier
        { vehicle_identifier: waypoint_params[:vehicle_identifier] }
      end

      # rubocop:disable Metrics/LineLength
      # Find or creates (by using save) a new Vehicle record.
      # If the task encounters some errors and the record can not be saved
      # then fires an implicit return and a 422 status
      def find_or_create_vehicle
        vehicle = Vehicle.find_or_initialize_by identifier
        return render json: { errors: vehicle.errors }, status: 422 unless vehicle.save
      end
    end
  end
end
