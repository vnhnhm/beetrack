class Vehicle < ApplicationRecord
  self.primary_key = 'vehicle_identifier'
  has_many :waypoints, foreign_key: :vehicle_identifier

  validates :vehicle_identifier, presence: true, uniqueness: true
end
