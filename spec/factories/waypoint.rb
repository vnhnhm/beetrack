FactoryBot.define do
  factory :waypoint do
    longitude '20.2'
    latitude '-0.56'
    sent_at '2016-06-02 20:45:00'
  end
end
