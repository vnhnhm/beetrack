class CreateWaypoints < ActiveRecord::Migration[5.1]
  def change
    create_table :waypoints do |t|
      t.string :latitude
      t.string :longitude
      t.datetime :sent_at
      t.string :vehicle_identifier, index: true

      t.timestamps
    end

    add_foreign_key :waypoints, :vehicles, primary_key: :vehicle_identifier, column: :vehicle_identifier
  end
end
