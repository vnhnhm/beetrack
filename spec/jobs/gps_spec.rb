require 'rails_helper'

RSpec.describe GpsWorker, type: :job do
  it 'matches with enqueued job' do
    ActiveJob::Base.queue_adapter = :test
    expect do
      GpsWorker.perform_async
    end.to change(GpsWorker.jobs, :size).by 1
  end
end
