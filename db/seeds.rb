Vehicle.destroy_all
Vehicle.create vehicle_identifier: 'AS1293'
Waypoint.destroy_all
Waypoint.create(
  latitude: 20.23, longitude: -0.56, sent_at: '2016-06-02 20:45:00',
  vehicle_identifier: 'AS1293'
)
