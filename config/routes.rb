require 'sidekiq/web'

Rails.application.routes.draw do
  mount Sidekiq::Web => '/sidekiq'
  namespace :api do
    namespace :v1 do
      post 'gps', to: 'gps#gps'
    end
  end
  get 'show/:id', to: 'api/v1/gps#show'
end
