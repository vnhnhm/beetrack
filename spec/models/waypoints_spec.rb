require 'rails_helper'

RSpec.describe Waypoint, type: :model do
  let(:vehicle) { create :vehicle }
  subject { build :waypoint, vehicle_identifier: vehicle.id }

  context 'is valid' do
    it 'with valid attributes' do
      expect(subject).to be_valid
    end
  end

  context 'is invalid' do
    after { expect(subject).to_not be_valid }
    specify('without longitude') { subject.longitude = nil }
    specify('without latitude')  { subject.latitude  = nil }
    specify('without send time') { subject.sent_at   = nil }
    specify('without a vehicle') { subject.vehicle_identifier = nil }
  end
end