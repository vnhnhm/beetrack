class Waypoint < ApplicationRecord
  belongs_to :vehicle, foreign_key: :vehicle_identifier

  validates_presence_of :longitude, :latitude, :sent_at
end
